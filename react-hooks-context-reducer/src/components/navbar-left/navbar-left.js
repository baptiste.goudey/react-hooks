import React from "react";
import './navbar-left.scss';
import { Link } from "react-router-dom";

const NavbarLeft = props => {
    return (
        <div className="sidebar-sticky">
            <ul className="nav flex-column">
                <li className="nav-item">
                    <Link className="nav-link" to="/">Add User</Link>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#!">
                        Exemple
            </a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#!">

                        Exemple
            </a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#!">

                        Exemple
            </a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#!">

                        Exemple
            </a>
                </li>
            </ul>
        </div>
    )
}

export default NavbarLeft;