import React from "react";
import logo from "../../logo.svg";
import './navbar.scss';

const Navbar = props => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{height : "60px"}} >
            <a className="navbar-brand" href="#!">
                <img src={logo} height="30" alt="logo" ></img>
            </a>
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                    <a className="btn btn-primary" href="#!">Mon compte</a>
                </li>
            </ul>
        </nav>
    )
}

export default Navbar;