import React, { useState, useContext } from 'react';
import { store } from './../contexts/PatientContext';
import { Redirect } from 'react-router-dom';

const FirstPage = () => {

    const { dispatch } = useContext(store);

    const [patient, setPatient] = useState({
        firstname: "",
        lastname: ""
    });

    const [validate, setValidate] = useState(false);

    const handleChange = (event) => {
        const value = event.currentTarget.value;
        const name = event.currentTarget.name;

        setPatient({ ...patient, [name]: value });
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        dispatch({ type: "UPDATE", payload: patient });
        setValidate(true);

    }

    if (validate === true) {
        return <Redirect to='/second-page' />
    }
    else {
        return (
            <div className="row justify-content-center">
                <div className="col-12 text-center">Peux-tu indiquer l'identité du patient ?</div>
                <form className="col-6 mt-5" onSubmit={handleSubmit} >
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Nom</label>
                        <input type="text" className="form-control" id="lastname" name="lastname" onChange={handleChange} value={patient.lastname} placeholder="Enter name" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Prenom</label>
                        <input type="text" className="form-control" id="firstname" name="firstname" onChange={handleChange} value={patient.firstname} placeholder="Password" />
                    </div>

                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

export default FirstPage;
