import React, { useContext, useState } from 'react';
import { store } from './../contexts/PatientContext';
import { Redirect } from 'react-router-dom';

const SecondPage = () => {
    const { dispatch } = useContext(store);

    const [validate, setValidate] = useState(false);


    const handleClick = (event, sexe) => {
        event.preventDefault();
        dispatch({ type: "UPDATE SEXE", payload: { sexe } });
        setValidate(true);
    }

    if (validate === true) {
        return <Redirect to='/third-page' />
    }
    else {

        return (
            <div className="row">
                <div className="col-12 text-center">Peux-tu indiquer ton sexe ?</div>
                <div className="col-12">
                    <div className="row text-center mt-5">
                        <div className="col-6">
                            <button onClick={(e) => handleClick(e, "Homme")} className="btn btn-primary">Homme</button>
                        </div>
                        <div className="col-6">
                            <button onClick={(e) => handleClick(e, "Femme")} className="btn btn-primary">Femme</button>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default SecondPage;
