import React, { useContext } from 'react';
import { store } from './../contexts/PatientContext';


const ThirdPage = props => {
    const { state } = useContext(store);

    if (!state.sexe || !state.lastname || !state.firstname) {
        return (
            <div className="row">
                <div className="col-12 text-center"> ERREUR LORS DE L'AJOUT DU PATIENT</div>
            </div>
        )
    }

    else {
        return (
            <div className="row">
                <div className="col-12 text-center"> {state.sexe === "Homme" ? "Monsieur" : "Madame"} {state.lastname} {state.firstname} </div>
            </div>
        )
    }
}

export default ThirdPage;
