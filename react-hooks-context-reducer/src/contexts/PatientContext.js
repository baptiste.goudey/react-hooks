import React, { createContext, useReducer } from 'react';

const initialState = {
    lastname: null,
    firstname: null,
    sexe: null
};
const store = createContext(initialState);
const { Provider } = store;

const StateProvider = ({ children }) => {
    const [state, dispatch] = useReducer((state, action) => {
        switch (action.type) {
            case "UPDATE":
                return ({
                    ...state,
                    firstname: action.payload.firstname,
                    lastname: action.payload.lastname,
                    sexe: action.payload.sexe
                });
            case "UPDATE SEXE":
                return ({
                    ...state,
                    sexe: action.payload.sexe
                });
            default:
                throw new Error();
        };
    }, initialState);

    return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider }