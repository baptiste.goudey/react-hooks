import React, { useContext } from 'react';
import PatientContext from './../contexts/PatientContext';


const ThirdPage = props => {
    const { patient } = useContext(PatientContext);

     if (!patient.sexe || !patient.lastname || !patient.firstname) {
        return (
            <div className="row">
                <div className="col-12 text-center"> ERREUR LORS DE L'AJOUT DU PATIENT</div>
            </div>
        )
    }

    else {
        return (
            <div className="row">
                <div className="col-12 text-center"> {patient.sexe === "Homme" ? "Monsieur" : "Madame"} {patient.lastname} {patient.firstname} </div>
            </div>
        )
    }
}

export default ThirdPage;
