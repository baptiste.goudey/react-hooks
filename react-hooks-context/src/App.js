import React, { useState } from 'react';
import './App.css';
import Navbar from './components/navbar/navbar';
import NavbarLeft from './components/navbar-left/navbar-left';
import { Switch, Route, BrowserRouter as Router, Redirect } from 'react-router-dom';
import FirstPage from './pages/first-page';
import SecondPage from './pages/second-page';
import ThirdPage from './pages/third-page';
import PatientContext from './contexts/PatientContext';

function App() {

  const [patient, setPatient] = useState({
    lastname : "",
    firstname : "",
    sexe : ""
  })

  const contextValue = {
    patient,
    setPatient
  }

  return (
    <PatientContext.Provider  value={contextValue} >
      <Router>
        <React.Fragment>
          <Navbar />
          <div className="container-fluid">
            <div className="row">
              <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                <NavbarLeft />
              </nav>
              <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                <Switch>
                  <Redirect from="/" exact to="/first-page" />
                  <Route path="/first-page" component={FirstPage} />
                  <Route path="/second-page" component={SecondPage} />
                  <Route path="/third-page" component={ThirdPage} />
                </Switch>
              </main>
            </div>
          </div>
        </React.Fragment>
      </Router>
    </PatientContext.Provider>
  );
}

export default App;
